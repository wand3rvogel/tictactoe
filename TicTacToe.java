package tictactoe;

import java.util.Arrays;
import java.util.Scanner;

public class TicTacToe {

    private static char[][] symbols;
    private static final char EMPTY = '_';
    private static final int SIZE = 3;

    public static void fillBoard() {

        symbols = new char[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++) {
            Arrays.fill(symbols[i], '_');
        }
    }

    public static void printBoard() {

        System.out.println("---------");
        for (int i = 0; i < SIZE; i++) {
            System.out.print("| ");
            for (int j = 0; j < SIZE; j++) {
                System.out.print(symbols[i][j] + " ");
            }
            System.out.println("|");
        }
        System.out.println("---------");
    }

    public static boolean testEmptyCells() {

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (symbols[i][j] == EMPTY) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean testThreeInARow(char player) {

        // three in rows
        for (int i = 0; i < SIZE; i++) {
            int count = 0;
            for (int j = 0; j < SIZE; j++) {
                if (symbols[i][j] == player) {
                    count++;
                }
            }
            if (count == SIZE) {
                return true;
            }
        }

        // three in cols
        for (int i = 0; i < SIZE; i++) {
            int count = 0;
            for (int j = 0; j < SIZE; j++) {
                if (symbols[j][i] == player) {
                    count++;
                }
            }
            if (count == SIZE) {
                return true;
            }
        }

        // diagonal 1
        int count = 0;
        for (int i = 0; i < SIZE; i++) {
            if (symbols[i][i] == player) {
                count++;
            }
        }
        if (count == SIZE) {
            return true;
        }

        // diagonal 2
        count = 0;
        for (int i = 0; i < SIZE; i++) {
            if (symbols[SIZE - i - 1][i] == player) {
                count++;
            }
        }
        if (count == SIZE) {
            return true;
        }

        return false;
    }

    public static boolean isDone() {

        boolean hasEmpty = testEmptyCells();
        boolean xWins = testThreeInARow('X');
        boolean oWins = testThreeInARow('O');

        boolean hasWinner = xWins || oWins;

        if (hasEmpty && !hasWinner) {
            return false;
        }
        if (!hasEmpty && !hasWinner) {
            System.out.println("Draw");
        } else if (xWins) {
            System.out.println("X wins");
        } else if (oWins) {
            System.out.println("O wins");
        }
        return true;
    }

    public static void makeMove(char player) {

        Scanner scanner = new Scanner(System.in);

        boolean isAllOk = false;
        int num1, num2;

        while (!isAllOk) {
            System.out.print("Enter the coordinates: ");

            boolean innerError = false;
            while (!scanner.hasNextInt()) {
                System.out.println("You should enter numbers!");
                scanner.nextLine();
                innerError = true;
                break;
            }
            if (innerError) {
                continue;
            }
            num1 = scanner.nextInt();
            num2 = scanner.nextInt();
            scanner.nextLine();

            if (num1 < 1 || num1 > 3 || num2 < 1 || num2 > 3) {
                System.out.println("Coordinates should be from 1 to 3!");
                continue;
            }

            if (symbols[num1 - 1][num2 - 1] != EMPTY) {
                System.out.println("This cell is occupied! Choose another one!");
                continue;
            }
            isAllOk = true;
            symbols[num1 - 1][num2 - 1] = player;
        }
    }

    public static void main(String[] args) {

        fillBoard();
        printBoard();

        char currentPlayer = 'X';
        do {
            makeMove(currentPlayer);
            printBoard();
            currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
        } while (!isDone());
    }
}
